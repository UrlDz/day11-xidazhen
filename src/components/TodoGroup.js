import TodoItem from "./TodoItem";

const TodoGroup = ({ todos }) => {
    return (
        <ul>
            {todos.map((todo, index) => (
                <TodoItem key={index} todo={todo} />
            ))}
        </ul>
    );
};

export default TodoGroup;
