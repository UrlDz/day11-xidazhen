
const TodoItem = ({ todo }) => {
    return <li><input type="text" value={todo} disabled /></li>;
};
export default TodoItem;