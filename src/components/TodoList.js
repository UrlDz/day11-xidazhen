import { useState } from 'react';
import TodoGenerator from "./TodoGenerator";
import TodoGroup from "./TodoGroup";

const TodoList = () => {
    const [todos, setTodos] = useState([]);

    const addTodo = (todo) => {
        setTodos([...todos, todo]);
    };

    return (
        <div>
            <h1>Todo List</h1>
            <TodoGroup todos={todos} />
            <TodoGenerator addTodo={addTodo} />

        </div>
    );
};
export default TodoList;